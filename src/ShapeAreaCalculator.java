import java.util.Scanner;

public class ShapeAreaCalculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // 用户选择形状
        System.out.println("请输入形状名称（正方形、圆形、三角形 或 梯形）：");
        String shape = scanner.nextLine().trim().toLowerCase();

        // 根据用户选择调用相应的方法
        if ("正方形".equals(shape)) {
            calculateSquareArea(scanner);
        } else if ("圆形".equals(shape)) {
            calculateCircleArea(scanner);
        } else if ("三角形".equals(shape)) {
            calculateTriangleArea(scanner);
        } else if ("梯形".equals(shape)) {
            calculateTrapezoidArea(scanner);
        } else {
            System.out.println("不支持的形状类型。");
        }

        scanner.close();
    }

    // 计算正方形面积的方法
    public static void calculateSquareArea(Scanner scanner) {
        System.out.println("请输入正方形的边长：");
        double side = scanner.nextDouble();
        System.out.println("正方形的面积是：" + squareArea(side));
    }

    // 计算圆形面积的方法

    public static void calculateCircleArea(Scanner scanner) {
        System.out.println("请输入圆形的半径：");
        double radius = scanner.nextDouble();
        System.out.println("圆形的面积是：" + circleArea(radius));
    }

    // 计算三角形面积的方法


    public static void calculateTriangleArea(Scanner scanner) {
        System.out.println("请输入三角形的三条边长：");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        System.out.println("三角形的面积是：" + triangleArea(a, b, c));
    }

    // 计算梯形面积的方法
    public static void calculateTrapezoidArea(Scanner scanner) {
        System.out.println("请输入梯形的上底长度：");
        double base1 = scanner.nextDouble();
        System.out.println("请输入梯形的下底长度：");
        double base2 = scanner.nextDouble();
        System.out.println("请输入梯形的高：");
        double height = scanner.nextDouble();
        System.out.println("梯形的面积是：" + trapezoidArea(base1, base2, height));
    }

    // 静态方法：计算正方形面积
    public static double squareArea(double side) {
        return side * side;
    }

    // 静态方法：计算圆形面积
    public static double circleArea(double radius) {
        return Math.PI * radius * radius;
    }

    // 静态方法：计算三角形面积（使用海伦公式）
    public static double triangleArea(double a, double b, double c) {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    // 静态方法：计算梯形面积
    public static double trapezoidArea(double base1, double base2, double height) {
        return (base1 + base2) * height / 2;
            }
    }


